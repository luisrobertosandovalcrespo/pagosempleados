/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pagosEmpleados;

/**
 *
 * @author luisr
 */
public class PagosEmpleados {
private int numDocente;
private String nombre;
private String domicilio;
private int nivel;
private float pagoHora;
private int totalHoras;
      
public PagosEmpleados(){
    this.nivel= 0;
    this.numDocente= 0;
    this.totalHoras= 0;
    this.domicilio="";
    this.nombre="";
    this.pagoHora= 0.0f;
    
    
}
public PagosEmpleados (int nivel, String domicilio, String nombre, int numDocente, int totalHoras, float pagoHora){
    this.domicilio= domicilio;
    this.nivel= nivel;
    this.nombre= nombre;
    this.numDocente= numDocente;
    this.pagoHora= pagoHora;
    this.totalHoras= totalHoras;
    
}
public PagosEmpleados(PagosEmpleados otro) {
    this.domicilio= otro.domicilio;
    this.nivel= otro.nivel;
    this.nombre=otro.nombre;
    this.numDocente=otro.numDocente;
    this.pagoHora=otro.pagoHora;
    this.totalHoras=otro.totalHoras;
}

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(float pagoHora) {
        this.pagoHora = pagoHora;
    }

    public int getTotalHoras() {
        return totalHoras;
    }

    public void setTotalHoras(int totalHoras) {
        this.totalHoras = totalHoras;
    }

    public void CalcularPago(){
        
    }
    
    public float CalculadoraPago(int nivel, float pagoHora, int totalHoras) {
        
    float pagoTotal = 0;

        
        float porcentajeIncremento;
        switch (nivel) {
            case 1:
                porcentajeIncremento = 0.30f;
                break;
            case 2:
                porcentajeIncremento = 0.50f;
                break;
            case 3:
                porcentajeIncremento = 1.00f;
                break;
            default:
                porcentajeIncremento = 0; 
        }

      
        float pagoHoraConIncremento = pagoHora * (1 + porcentajeIncremento);

       
        pagoTotal = pagoHoraConIncremento * totalHoras;

        return pagoTotal;
    }
    
     public float calcularImpuesto(float pagoTotal) {
         pagoTotal= pagoTotal * 0.16f;
        return pagoTotal ;
    }

    
    public float calcularBono(int cantidadHijos) {
        float bono = 0;

        // Determinar el porcentaje de bono según la cantidad de hijos
        if (cantidadHijos >= 1 && cantidadHijos <= 2) {
            bono = 0.05f;
        } else if (cantidadHijos >= 3 && cantidadHijos <= 5) {
            bono = 0.10f;
        } else if (cantidadHijos > 5) {
            bono = 0.20f;
        }

        return bono;
    }

    
}
    
    
    




    
        