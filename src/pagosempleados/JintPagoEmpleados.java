
package PagosEmpleados;

import javax.swing.JOptionPane;

/**
 *
 * @author elpep
 */
public class JintPagoEmpleados extends javax.swing.JInternalFrame {

  
    public JintPagoEmpleados() {
        initComponents();
        this.desabilitar();
        this.resize(907,504);
    }
    

    public void desabilitar(){
            this.txtNumDocente.setEnabled(false);
            this.txtPagoHora.setEnabled(false);
            this.txtDomicilio.setEnabled(false);
          
            this.txtPagoImpuesto.setEnabled(false);
            this.txtNombre.setEnabled(false);
            this.txtNivel.setEnabled(false);
            this.txtPagoTotal.setEnabled(false);
            this.txtTotalHoras.setEnabled(false);
            
            //botones
            this.btnGuardar.setEnabled(false);
            this.btnMostrar.setEnabled(false);
            
    }
    public void habilitar(){
            this.txtNumDocente.setEnabled(!false);
            this.txtPagoHora.setEnabled(!false);
            this.txtDomicilio.setEnabled(!false);
          
            this.txtNombre.setEnabled(!false);
            this.txtNivel.setEnabled(!false);
            this.txtTotalHoras.setEnabled(!false);
            this.btnGuardar.setEnabled(true);
    }
    public void limpiar(){
    
            this.txtNumDocente.setText("");
            this.txtPagoHora.setText("");
            this.txtDomicilio.setText("");
          
            this.txtPagoImpuesto.setText("");
            this.txtNombre.setText("");
            this.txtNivel.setText("");
            this.txtPagoTotal.setText("");
            this.txtTotalHoras.setText("");
            
            this.txtNumDocente.requestFocus();
    }
    
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        numDocente = new javax.swing.JLabel();
        nombre = new javax.swing.JLabel();
        domicilio = new javax.swing.JLabel();
        pagoHora = new javax.swing.JLabel();
        txtPagoHora = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        txtNivel = new javax.swing.JTextField();
        txtDomicilio = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txtPagoImpuesto = new javax.swing.JTextField();
        pagoImpuesto = new javax.swing.JLabel();
        txtPagoTotal = new javax.swing.JTextField();
        nivel = new javax.swing.JLabel();
        txtNumDocente = new javax.swing.JTextField();
        btnCerrar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnMostrar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        totalHoras = new javax.swing.JLabel();
        txtTotalHoras = new javax.swing.JTextField();

        setBackground(new java.awt.Color(0, 51, 153));
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Factura");
        addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                formAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        getContentPane().setLayout(null);

        numDocente.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        numDocente.setForeground(new java.awt.Color(153, 255, 255));
        numDocente.setText("Numero de Docente :");
        getContentPane().add(numDocente);
        numDocente.setBounds(60, 30, 180, 30);

        nombre.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        nombre.setForeground(new java.awt.Color(0, 255, 255));
        nombre.setText("Nombre del Docente:");
        getContentPane().add(nombre);
        nombre.setBounds(60, 100, 180, 30);

        domicilio.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        domicilio.setForeground(new java.awt.Color(51, 255, 255));
        domicilio.setText("Domicilio:");
        getContentPane().add(domicilio);
        domicilio.setBounds(140, 240, 90, 30);

        pagoHora.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        pagoHora.setForeground(new java.awt.Color(0, 255, 255));
        pagoHora.setText("Pago por Hora: ");
        getContentPane().add(pagoHora);
        pagoHora.setBounds(100, 310, 130, 30);

        txtPagoHora.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        getContentPane().add(txtPagoHora);
        txtPagoHora.setBounds(240, 310, 140, 30);

        txtNombre.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        getContentPane().add(txtNombre);
        txtNombre.setBounds(240, 100, 140, 30);

        txtNivel.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        getContentPane().add(txtNivel);
        txtNivel.setBounds(240, 170, 140, 30);

        txtDomicilio.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        getContentPane().add(txtDomicilio);
        txtDomicilio.setBounds(240, 240, 140, 30);

        jPanel2.setBackground(new java.awt.Color(153, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Informacion del Pago", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Trebuchet MS", 1, 18))); // NOI18N
        jPanel2.setLayout(null);

        jLabel3.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jLabel3.setText("Pago Total:");
        jPanel2.add(jLabel3);
        jLabel3.setBounds(110, 180, 100, 30);

        txtPagoImpuesto.setBackground(new java.awt.Color(204, 204, 204));
        txtPagoImpuesto.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        txtPagoImpuesto.setForeground(new java.awt.Color(255, 255, 255));
        txtPagoImpuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPagoImpuestoActionPerformed(evt);
            }
        });
        jPanel2.add(txtPagoImpuesto);
        txtPagoImpuesto.setBounds(220, 110, 140, 30);

        pagoImpuesto.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        pagoImpuesto.setText("Pago Impuesto :");
        jPanel2.add(pagoImpuesto);
        pagoImpuesto.setBounds(80, 110, 140, 30);

        txtPagoTotal.setBackground(new java.awt.Color(204, 204, 204));
        txtPagoTotal.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        txtPagoTotal.setForeground(new java.awt.Color(255, 255, 255));
        txtPagoTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPagoTotalActionPerformed(evt);
            }
        });
        jPanel2.add(txtPagoTotal);
        txtPagoTotal.setBounds(220, 180, 140, 30);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(400, 60, 430, 250);

        nivel.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        nivel.setForeground(new java.awt.Color(0, 255, 255));
        nivel.setText("Nivel:");
        getContentPane().add(nivel);
        nivel.setBounds(180, 170, 50, 30);

        txtNumDocente.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        txtNumDocente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNumDocenteActionPerformed(evt);
            }
        });
        getContentPane().add(txtNumDocente);
        txtNumDocente.setBounds(240, 30, 140, 30);

        btnCerrar.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnCerrar.setForeground(new java.awt.Color(0, 0, 204));
        btnCerrar.setText("Cerrar");
        btnCerrar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 51)));
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(700, 420, 80, 30);

        btnNuevo.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnNuevo.setForeground(new java.awt.Color(0, 0, 204));
        btnNuevo.setText("Nuevo");
        btnNuevo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 51)));
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(850, 30, 80, 50);

        btnGuardar.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnGuardar.setForeground(new java.awt.Color(0, 0, 204));
        btnGuardar.setText("Guardar");
        btnGuardar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 51)));
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardar);
        btnGuardar.setBounds(850, 110, 80, 50);

        btnMostrar.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnMostrar.setForeground(new java.awt.Color(0, 0, 204));
        btnMostrar.setText("Mostrar");
        btnMostrar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 51)));
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnMostrar);
        btnMostrar.setBounds(850, 190, 80, 50);

        btnLimpiar.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnLimpiar.setForeground(new java.awt.Color(0, 0, 204));
        btnLimpiar.setText("Limpiar");
        btnLimpiar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 51)));
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(440, 420, 80, 30);

        btnCancelar.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(0, 0, 204));
        btnCancelar.setText("Cancelar");
        btnCancelar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 51)));
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(570, 420, 80, 30);

        totalHoras.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        totalHoras.setForeground(new java.awt.Color(0, 255, 255));
        totalHoras.setText("Total de horas trabajadas:");
        getContentPane().add(totalHoras);
        totalHoras.setBounds(10, 350, 240, 70);

        txtTotalHoras.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        txtTotalHoras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalHorasActionPerformed(evt);
            }
        });
        getContentPane().add(txtTotalHoras);
        txtTotalHoras.setBounds(240, 370, 140, 30);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtPagoImpuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPagoImpuestoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPagoImpuestoActionPerformed

    private void txtTotalHorasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalHorasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalHorasActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        // TODO add your handling code here:
        
        int opcion = 0;
        opcion = JOptionPane.showConfirmDialog(this, "Desea salir?", "Pago", JOptionPane.YES_NO_OPTION);
        
        if (opcion == JOptionPane.YES_OPTION){
            this.dispose();
            
        }
        
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        this.habilitar();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        
        boolean factu = false;
            if (this.txtPagoHora.getText().equals("")) factu = true;
            if (this.txtDomicilio.getText().equals("")) factu = true;
            if (this.txtNumDocente.getText().equals("")) factu = true;
            
            if (this.txtNombre.getText().equals("")) factu = true;
            if (this.txtNivel.getText().equals("")) factu = true;
            if (this.txtTotalHoras.getText().equals("")) factu = true;
            
            if (factu == true){
            //falto informacion
            JOptionPane.showMessageDialog(this, "falto capturar informacion");
            }
            else{
                con.setNumFactura(Integer.parseInt(this.txtNumDocente.getText()));
                con.setDescripcion(this.txtPagoHora.getText());
                con.setDomicilioFiscal(this.txtDomicilio.getText());
                
                con.setNombreCliente(this.txtNombre.getText());
                con.setRfc(this.txtNivel.getText());
                con.setTotalVenta(Float.parseFloat(this.txtTotalHoras.getText()));
              
                
                
                JOptionPane.showMessageDialog(this, "Se guardo con exito la informacion");
                this.btnMostrar.setEnabled(true);
            }
        
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        // TODO add your handling code here:
        
        this.txtPagoHora.setText(con.getDescripcion());
        this.txtDomicilio.setText(con.getDomicilioFiscal());
        
        this.txtNombre.setText(con.getNombreCliente());
        this.txtNivel.setText(con.getRfc());
        this.txtTotalHoras.setText(String.valueOf(con.getTotalVenta()));
        this.txtNumDocente.setText(String.valueOf(con.getNumFactura()));
        
        this.txtPagoImpuesto.setText(String.valueOf(con.calcularImpuesto()));
        this.txtPagoTotal.setText(String.valueOf(con.calcularTotalPagar()));
        
       
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        this.desabilitar();
        this.limpiar();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void txtNumDocenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNumDocenteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumDocenteActionPerformed

    private void txtPagoTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPagoTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPagoTotalActionPerformed

    private void formAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_formAncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_formAncestorAdded


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JLabel domicilio;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel nivel;
    private javax.swing.JLabel nombre;
    private javax.swing.JLabel numDocente;
    private javax.swing.JLabel pagoHora;
    private javax.swing.JLabel pagoImpuesto;
    private javax.swing.JLabel totalHoras;
    private javax.swing.JTextField txtDomicilio;
    private javax.swing.JTextField txtNivel;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtNumDocente;
    private javax.swing.JTextField txtPagoHora;
    private javax.swing.JTextField txtPagoImpuesto;
    private javax.swing.JTextField txtPagoTotal;
    private javax.swing.JTextField txtTotalHoras;
    // End of variables declaration//GEN-END:variables

    
}
